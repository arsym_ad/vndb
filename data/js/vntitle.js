function vnTitleChange() {
  // Serialize language table
  var c = [];
  var l = byName(byId('title_tbl'), 'tr');
  for (var i = 0; i < l.length; i++) {
    var lang  = byName(byClass(l[i], 'lang' )[0], 'input')[0];
    var title = byName(byClass(l[i], 'title')[0], 'input')[0];
    var latin = byName(byClass(l[i], 'latin')[0], 'input')[0];
    var official = byName(byClass(l[i], 'official')[0], 'input')[0];
    if (title.value == "")
      continue;
    
    var latin_val = latin.value == "" ? null : latin.value;
    var official_int = official.checked ? 1:0;
    
    c.push({ lang:lang.value, title:title.value, latin:latin_val, official:official_int });
  }
  
  byId('title_languages').value = JSON.stringify(c);
  vnLanguageUpdate();
  return true;
}

function vnLanguageUpdate() {
  // Update title and original hidden fields
  var olang = byId('olang').value;
  var title = byId('title_'+olang).value;
  var latin = byId('latin_'+olang).value;
  
  byId('title').value    = latin ? latin : title;
  byId('original').value = latin ? title : '';
}

// VN Editing title
if(byId('title_tbl'))
{
  // Populate original language entry
  var olang = byId('olang').value;
  var title = byId('title').value;
  var original = byId('original').value;
  
  byId('title_'+olang).value = original ? original : title;
  byId('latin_'+olang).value = original ? title : '';
  byId('official_'+olang).checked = true;
  
  // Add hooks on change
  byId('title_tbl').onchange = vnTitleChange;
  byId('olang').onchange = vnLanguageUpdate;
  vnTitleChange();
}